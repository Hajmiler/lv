<html>
    <head>

    </head>
    
    <body>
        
        <?php
        $student=array("Ivan","Hajmiler","99999","AI4544","3. godina");
        echo "Student: <br>";
        echo "<br>";
        echo "Ime: ".$student[0]."<br>";
        echo "Prezime: ".$student[1]. "<br>";
        echo "JMBG: ".$student[2]. "<br>";
        echo "Broj indeksa: ".$student[3]. "<br>";
        echo "Godina studija: ".$student[4]. "<br>";
        echo "<br>";
        
        $automobil=array(
            "Citroen"=>array("c4","1.6","metalik siva","2014.","70kW"),
            "Mercedes"=>array("c","2.0","metalik crna","2016.","120kw")
        );
        
        echo "<h1>Automobili</h1> <br>";
        
        echo "Citroen <br>";
        echo "<br>";
        echo "Tip automobila: ".$automobil["Citroen"][0]."<br>";
        echo "Kubikaža: ".$automobil["Citroen"][1]."<br>";
        echo "Boja: ".$automobil["Citroen"][2]."<br>";
        echo "Godina proizvodnje: ".$automobil["Citroen"][3]."<br>";
        echo "Motor: ".$automobil["Citroen"][4]."<br>";
        echo "<br>";

        echo "Mercedes <br>";
        echo "<br>";
        echo "Tip automobila: ".$automobil["Mercedes"][0]."<br>";
        echo "Kubikaža: ".$automobil["Mercedes"][1]."<br>";
        echo "Boja: ".$automobil["Mercedes"][2]."<br>";
        echo "Godina proizvodnje: ".$automobil["Mercedes"][3]."<br>";
        echo "Motor: ".$automobil["Mercedes"][4]."<br>";
        echo "<br>";
        
        ?>
    </body>
</html>